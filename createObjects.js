let person = {
    "name ?/": "Andrew",
    "age": 43,
    5: true,
};

let propertyName = "name";

console.log(person.name);
console.log(person["age"]);
console.log(person[propertyName]);
console.log(person["name ?/"]);