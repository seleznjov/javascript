const people = [
    {
        name: "Andrew",
        age: 43,
    },

    {
        name: "Nik",
        age: 25,
    }
];

console.log(people.find(element => {

    return element.age === 25
}));