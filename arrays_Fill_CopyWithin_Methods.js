const zeroes = [0, 0, 0, 0, 0, 0];
const array = ["sa", "ds", "dsffsdf", "sds", "sds","sdrs","sdgts","sdser","sdds"];

zeroes.fill(5);
console.log(zeroes);
zeroes.fill(0);
console.log(zeroes);

zeroes.fill(7, 2, 4);
console.log(zeroes);

//*********************************************************************************************************************

let ints, reset;

reset = () => ints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
reset();

ints.copyWithin(5);
console.log(ints);
reset();
array.copyWithin(0, 6);
console.log(array);
