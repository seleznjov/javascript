const a = ["foo", "bar", "baz", "qux"];

const aKeys = Array.from(a.keys());
const aElements = Array.from(a.values());
const aElemValue = Array.from(a.entries());

console.log(aKeys);
console.log(aElements);
console.log(aElemValue);